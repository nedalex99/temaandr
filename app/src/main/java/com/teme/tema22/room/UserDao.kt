package com.teme.tema22.room

import androidx.room.*

@Dao
public interface UserDao {
    @Query("SELECT * FROM user")
    fun getAll(): List<User>

    @Insert
    fun insert(insert: User)

    @Update
    fun update(update: User)

    @Delete
    fun delete(delete: User)

    @Query("DELETE FROM user")
    fun deleteAll()

    @Query("DELETE from user where first_name like :firstName AND last_name like :lastName")
    fun deleteUser(firstName: String, lastName: String)

}