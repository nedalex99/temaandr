package com.teme.tema22.ui

import android.os.AsyncTask
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.GsonBuilder
import com.teme.tema22.Adapter

import com.teme.tema22.R
import com.teme.tema22.room.AppDatabase
import com.teme.tema22.room.User
import com.teme.tema22.room.Users
import kotlinx.android.synthetic.main.fragment_one.*
import okhttp3.*
import java.io.IOException
import kotlin.random.Random

/**
 * A simple [Fragment] subclass.
 */
class FragmentOne : Fragment() {

    private lateinit var users: List<User>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_one, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        AppDatabase.getAppDatabase(activity!!)

        recycler_view.layoutManager = LinearLayoutManager(activity)

        function()

        btn_add_user.setOnClickListener {
            val firstName = tv_first_name.text.toString()
            val lastName = tv_last_name.text.toString()

            var user = User(Random.nextInt(1000000), firstName, lastName)
            saveUser(user)
            //function()
        }

        btn_remove_user.setOnClickListener {
            val firstName = tv_first_name.text.toString()
            val lastName = tv_last_name.text.toString()

            var user = User(Random.nextInt(1000000), firstName, lastName)
            deleteUser(user)
        }

        btn_sync.setOnClickListener {
            setUpUserListFromServer()
        }

    }

    private fun setUpUserListFromServer(){
        val url = "https://jsonplaceholder.typicode.com/users"

        val request = Request.Builder().url(url).build()

        val client = OkHttpClient()

        var userz: List<User>

        client.newCall(request).enqueue(object : Callback{
            override fun onFailure(call: Call, e: IOException) {
                println("Failure")
            }

            override fun onResponse(call: Call, response: Response) {
                val body = response.body()?.string()

                val gson = GsonBuilder().create()

                userz = gson.fromJson(body, Users::class.java)

                recycler_view.adapter = Adapter(userz)
            }
        })

    }

    private fun saveUser(user: User){
        AsyncTask.execute{
            AppDatabase.getAppDatabase(activity!!).userDao().insert(user)
            users = AppDatabase.getAppDatabase(activity!!).userDao().getAll()



            //Toast.makeText(activity,"It worked", Toast.LENGTH_LONG).show()
        }
        recycler_view.adapter = Adapter(users)
    }

    private fun function(){
        AsyncTask.execute{
            users = AppDatabase.getAppDatabase(activity!!).userDao().getAll()
            recycler_view.adapter = Adapter(users)
        }
    }

    private fun deleteUser(user: User){
        AsyncTask.execute {
            AppDatabase.getAppDatabase(activity!!).userDao().deleteUser(user.firstName, user.lastName)
            users = AppDatabase.getAppDatabase(activity!!).userDao().getAll()

        }
        recycler_view.adapter = Adapter(users)
    }

}
