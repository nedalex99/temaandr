package com.teme.tema22.ui

import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import com.teme.tema22.R
import com.teme.tema22.room.AppDatabase
import com.teme.tema22.room.User
import com.teme.tema22.room.UserDao
import kotlinx.android.synthetic.main.fragment_one.*
import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    //lateinit var db: AppDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.fl_container, FragmentOne(), "FragmentOne")
        transaction.commit()

    }



}
